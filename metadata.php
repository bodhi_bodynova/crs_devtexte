<?php
/**
 * @TODO LICENCE HERE
 */

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id'          => 'crs_devtexte',
    'title'       => array(
        'de' => 'Christians erstes Textemodul',
        'en' => 'OXID6 example module',
    ),
    'description' => array(
        'de' => '<h2>Textemodul für Bodynova</h2>',
        'en' => '<h2>OXID6 example module</h2>',
    ),
    'thumbnail'   => 'out/img/linslin-org-logo.png',
    'version'     => '1.0.0',
    'author'      => 'Christian Ewald',
    'url'         => 'https://bodynova.de',
    'email'       => 'c.ewald@bodynova.de',
    'extend'      => array(
    ),
    'controllers'       => array(
        //'linslinexamplemodulemain' => \linslin\oxid6ExampleModule\Controller\Admin\MainController::class,
    ),
    'files'       => array(),
    'templates'   => array(
        //'main.tpl' => 'linslin/oxid6-example-module/views/admin/main.tpl'
    ),
    'blocks'      => array(),
    'settings'    => array(),
    'events'      => array(),
);
